package com.topmap.service.impl;

import com.topmap.dao.UserDOMapper;
import com.topmap.dao.UserPasswordDOMapper;
import com.topmap.dataobject.UserDO;
import com.topmap.dataobject.UserPasswordDO;
import com.topmap.error.BusinessException;
import com.topmap.error.EmBusinessError;
import com.topmap.service.UserService;
import com.topmap.service.model.UserModel;
import com.topmap.validator.ValidationResult;
import com.topmap.validator.ValidatorImpl;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DuplicateKeyException;
import org.springframework.stereotype.Service;

/**
 * @Author: MaHuadong
 * @Date: 2019/3/27 16:10
 * 具体逻辑的实现层
 * @Version 1.0
 */
@Service
public class UserServiceImpl implements UserService{

    @Autowired
    private UserDOMapper userDOMapper;
    @Autowired
    private UserPasswordDOMapper userPasswordDOMapper;

    //校验
    @Autowired
    private ValidatorImpl validator;




    //通过用户id获取用户信息的具体逻辑
    @Override
    public UserModel getUserById(Integer id) {
        //调用userDOMapper获取到对应用户的dataobject
        UserDO userDO= userDOMapper.selectByPrimaryKey(id);
        //现在我们需要通过用户的id获取用户的密码，但是默认的Mapper里没有这个方法，所以我们要去Mapping里写这个方法
        if (userDO==null){
            return null;
        }
        //通过用户id获取对应用户的加密密码信息
        UserPasswordDO userPasswordDO= userPasswordDOMapper.selectByUserId(userDO.getId());

        //这个方法的目的是根据用户id返回具体业务逻辑操作的UserModel，现在我们根据userDOMapper获取
        //到了UserDO，现在只需要写一个方法将UserDO转换为UserModel即可

        return convertFromUserDO(userDO,userPasswordDO);
    }

    //用户注册的具体实现逻辑
    @Override
    public void register(UserModel userModel) throws BusinessException {
        if (userModel==null){
            throw new BusinessException(EmBusinessError.PARAMETER_VALIDATION_ERROR);
        }
        ValidationResult result= validator.validator(userModel);
        if (result.isHasError()){
            throw new BusinessException(EmBusinessError.PARAMETER_VALIDATION_ERROR,result.getErrorMsg());
        }

        //在这里我们要实现model转换成do的方法,因为我们dao层的mapper识别的是dataobject（do）文件
        UserDO userDO=convertFromUserModel(userModel);
        //把信息插入数据库
        try {
            userDOMapper.insertSelective(userDO);   //这个就没有问题
        }catch (DuplicateKeyException ex){
            throw new BusinessException(EmBusinessError.PARAMETER_VALIDATION_ERROR,"手机号重复注册！");
        }
        userModel.setId(userDO.getId());
        UserPasswordDO userPasswordDO=convertPasswordFromUserModel(userModel);
        userPasswordDOMapper.insertSelective(userPasswordDO);
        return;
    }


    //用户登录的具体实现逻辑
    @Override
    public UserModel validateLogin(String telphone, String encrptPassword) throws BusinessException {
        //通过用户的手机获取用户信息sql
        UserDO userDO= userDOMapper.selectByTelphone(telphone);
        //判空处理
        if (userDO==null){
            throw new BusinessException(EmBusinessError.USER_LOGIN_FAIL);
        }
        //通过userdo的id拿到userpassworddo的id，然后获取密码信息
        //拿到密码信息
        UserPasswordDO userPasswordDO=userPasswordDOMapper.selectByUserId(userDO.getId());
        //通过userdo拿到usermodel
        UserModel userModel=convertFromUserDO(userDO,userPasswordDO);
        //对比用户信息内加密的密码是否和传输进来的密码想匹配
        if (!StringUtils.equals(encrptPassword,userModel.getEncrptPassword())){
            throw new BusinessException(EmBusinessError.USER_LOGIN_FAIL);
        }
        return userModel;
    }



    //在这里我们要实现model转换成do的方法,因为我们dao层的mapper识别的是dataobject（do）文件
    //参数是UserModel，返回值是UserDO
    private UserDO convertFromUserModel(UserModel userModel){
        //非空判断
        if (userModel==null){
            return  null;
        }
        UserDO userDO=new UserDO();
        //  简单的使用copyProperties把对应usermodel的属性copy到userDO类中
        BeanUtils.copyProperties(userModel,userDO);

        return userDO;
    }

    //不要忘了还有密码的转换
    private UserPasswordDO convertPasswordFromUserModel(UserModel userModel){
        //非空判断
        if (userModel==null){
            return  null;
        }
        UserPasswordDO userPasswordDO=new UserPasswordDO();
        // // 不需要使用copyProperties，直接set即可,不要忘了setUserId，不然外界找不到
        userPasswordDO.setEncrptPassword(userModel.getEncrptPassword());

        userPasswordDO.setUserId(userModel.getId());
        return userPasswordDO;

    }



    private UserModel convertFromUserDO(UserDO userDO, UserPasswordDO userPasswordDO){
        //为了使程序更加健壮，这里加上判空处理
        if (userDO==null){
            return null;
        }
        UserModel userModel=new UserModel();  //实例化一个UserModel
        //一个简单的copy
        BeanUtils.copyProperties(userDO,userModel);
        //由于model层还有一个encrptpassword字段，这个需要单独赋值,根据UserPassworDO获取
        //也是做一个判空处理
        if (userPasswordDO!=null){
            userModel.setEncrptPassword(userPasswordDO.getEncrptPassword());
        }

        return userModel;
    }
}
