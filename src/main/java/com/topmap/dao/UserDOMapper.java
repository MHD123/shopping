package com.topmap.dao;

import com.topmap.dataobject.UserDO;

public interface UserDOMapper {

    int deleteByPrimaryKey(Integer id);


    int insert(UserDO record);


    int insertSelective(UserDO record);


    UserDO selectByPrimaryKey(Integer id);

    //通过用户手机号查询用户信息
    UserDO selectByTelphone(String telphone);


    int updateByPrimaryKeySelective(UserDO record);


    int updateByPrimaryKey(UserDO record);
}