package com.topmap.controller;

import com.alibaba.druid.util.StringUtils;
import com.topmap.controller.viewobject.UserVO;
import com.topmap.error.BusinessException;
import com.topmap.error.EmBusinessError;
import com.topmap.response.CommonReturnType;
import com.topmap.service.UserService;
import com.topmap.service.model.UserModel;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import sun.misc.BASE64Encoder;

import javax.servlet.http.HttpServletRequest;
import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Random;

/**
 * @Author: MaHuadong
 * @Date: 2019/3/27 16:04
 * @Version 1.0
 */
@Controller("user")
@RequestMapping("/user")
//允许跨域
@CrossOrigin(allowCredentials = "true",allowedHeaders = "*")
public class UserController extends BaseController {

    @Autowired
    private UserService userService;

    //注入这个即可拿到Httpsession
    @Autowired
    private HttpServletRequest httpServletRequest;


    //用户注册接口
    @RequestMapping(value = "/register",method = {RequestMethod.POST},consumes={CONTENT_TYPE_FORMED})
    @ResponseBody
    //需要参数：手机号，验证码，用户名，性别，年龄
    public CommonReturnType register(@RequestParam(name = "telphone") String telphone,
                                     @RequestParam(name = "otpCode") String otpCode,
                                     @RequestParam(name = "name") String name,
                                     @RequestParam(name = "gender") Integer gender,
                                     @RequestParam(name = "age") Integer age,
                                     @RequestParam(name = "password") String password
    ) throws BusinessException, UnsupportedEncodingException, NoSuchAlgorithmException {
        //验证手机号和对应的otpCode相符
        String inSessionOtpCode= (String) this.httpServletRequest.getSession().getAttribute(telphone);
        //使用阿里巴巴的等值判别，好处是已经做了非空判断
        if (!StringUtils.equals(otpCode,inSessionOtpCode)){
            throw new BusinessException(EmBusinessError.PARAMETER_VALIDATION_ERROR,"短信验证码不符合要求！");
        }
        //若手机号与验证码相对应,则开始注册，需要一个service去执行注册流程
        UserModel userModel=new UserModel();
        userModel.setName(name);
        //转换gender的字符串类型
        userModel.setGender(new Byte(String.valueOf(gender.intValue())));
        userModel.setAge(age);
        userModel.setTelphone(telphone);
        userModel.setRegisterMode("byphone");

        //对密码做加密处理
        userModel.setEncrptPassword(this.EncodeByMd5(password));

        userService.register(userModel);

        return CommonReturnType.creat(null);

    }


    //用户登录接口
    @RequestMapping(value = "/login",method = {RequestMethod.POST},consumes={CONTENT_TYPE_FORMED})
    @ResponseBody
    public CommonReturnType login(@RequestParam(name = "telphone") String telphone,
                                  @RequestParam(name = "password") String password) throws BusinessException, UnsupportedEncodingException, NoSuchAlgorithmException {
        //首先入参校验
        if(org.apache.commons.lang3.StringUtils.isEmpty(telphone)||
                org.apache.commons.lang3.StringUtils.isEmpty(password)){
            throw new BusinessException(EmBusinessError.PARAMETER_VALIDATION_ERROR);
        }
        //用户登录服务，校验用户登录是否合法
        UserModel userModel=userService.validateLogin(telphone,this.EncodeByMd5(password));

        //将登录凭证加入到用户登录成功的session内
        /**
         * Session技术是一种将会话状态保存在服务器端的技术 ，
         * 它可以比喻成是医院发放给病人的病历卡和医院为每个病人保留的病历档案的结合方式 。
         客户端需要接收、记忆和回送 Session的会话标识号，
         Session可以且通常是借助Cookie来传递会话标识号*/
        this.httpServletRequest.getSession().setAttribute("IS_LOGIN",true);

        this.httpServletRequest.getSession().setAttribute("LOGIN_USER",userModel);
        //返回给前端正确的信息
        return CommonReturnType.creat(null);
    }


    public String EncodeByMd5(String str) throws NoSuchAlgorithmException, UnsupportedEncodingException {
        //确定一个计算方法
        /*java.security.MessageDigest类用于为应用程序提供信息摘要算法的功能，如 MD5 或 SHA 算法
         * */
        MessageDigest md5=MessageDigest.getInstance("MD5");
        BASE64Encoder base64Encoder=new BASE64Encoder();

        //加密字符传
        String newStr=base64Encoder.encode(md5.digest(str.getBytes("utf-8")));
        return  newStr;
    }

    //根据用户id请求用户信息接口
    @RequestMapping("/get/{id}")
    @ResponseBody
    // @ResponseBody是作用在方法上的，@ResponseBody 表示该方法的返回结果直接写入 HTTP response body 中
    // 比如异步获取 json 数据，加上 @ResponseBody 后，会直接返回 json 数据。
    public CommonReturnType getUser(@PathVariable(value = "id") Integer id) throws BusinessException {
        //调用service服务获取对应id的用户对象并返回给前端
        UserModel userModel=userService.getUserById(id);
        if (userModel==null){
            throw new BusinessException(EmBusinessError.USER_NOT_EXIT);
        }
        //将核心领域模型对象转换为可供UI使用的viewobject
        UserVO userVO= convertFromModel(userModel);
        return CommonReturnType.creat(userVO);

    }


    //用户获取otp短信的接口
    //根据用户id请求用户信息
    @RequestMapping(value = "/getotp",method = {RequestMethod.POST},consumes={CONTENT_TYPE_FORMED})
    @ResponseBody
    public CommonReturnType getOtp(@RequestParam(name = "telphone") String telphone){
        //1、需要按照一定的规则生成OTP验证码
        Random random=new Random();

        int randomInt=random.nextInt(99999);   //随机数取值[0,99999)
        randomInt=randomInt+10000;                     //随机数取值[10000,109999)

        String otpCode=String.valueOf(randomInt);


        //2、将OTP验证码同对应用户的手机号关联,使用Httpsession绑定手机号与验证码
        httpServletRequest.getSession().setAttribute(telphone,otpCode);

        //3、将OTP验证码通过短信的形式发送给用户,暂时省略，需要经费
        System.out.println("telphone="+telphone+"&otpCode="+otpCode);   //一般要用日志打印，我们现在为了方便这样用

        return CommonReturnType.creat(null);

    }

    //此方法是把usermodel对应的信息返回出去，值返回前端展示需要的数据,返回值是U
    private UserVO convertFromModel(UserModel userModel){
        if (userModel==null){
            return null;
        }
        //实例化
        UserVO userVO=new UserVO();
        //copy
        BeanUtils.copyProperties(userModel,userVO);
        //返回userVO
        return userVO;
    }
}
