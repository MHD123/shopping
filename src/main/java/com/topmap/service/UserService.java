package com.topmap.service;

import com.topmap.error.BusinessException;
import com.topmap.service.model.UserModel;

/**
 * @Author: MaHuadong
 * @Date: 2019/3/27 16:10
 * @Version 1.0
 */
public interface UserService {

    //通过用户对象ID获取用户对象的方法,返回值为业务逻辑对象UserModel
    UserModel getUserById(Integer id);

    //usermodel内完成用户注册的整套流程
    void register(UserModel userModel) throws BusinessException;

    //用户登录服务，校验用户登录是否合法
    //telphone：用户注册手机
    //password：用户加密后的密码
    UserModel validateLogin(String telphone,String encrptPassword) throws BusinessException;

}
