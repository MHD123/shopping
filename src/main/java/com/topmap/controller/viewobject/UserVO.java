package com.topmap.controller.viewobject;

/**
 * @Author: MaHuadong
 * @Date: 2019/3/28 14:46
 * @Version 1.0
 */
//把前端需要的字段返回给他，不需要的不给他,逻辑在model里面实现，这里只是供前端展示
public class UserVO {
    //前端只需要这五个字段展示就够了
    private Integer id;
    private String name;
    private Byte gender;
    private String telphone;
    private Integer age;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Byte getGender() {
        return gender;
    }

    public void setGender(Byte gender) {
        this.gender = gender;
    }

    public String getTelphone() {
        return telphone;
    }

    public void setTelphone(String telphone) {
        this.telphone = telphone;
    }

    public Integer getAge() {
        return age;
    }

    public void setAge(Integer age) {
        this.age = age;
    }
}
