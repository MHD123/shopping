package com.topmap.error;

/**
 * @Author: MaHuadong
 * @Date: 2019/4/2 14:54
 * @Version 1.0
 */
//包装器业务异常类实现
public class BusinessException  extends Exception implements CommonError{

    //强关联一个对应的CommonError，在这边commonError就是枚举EmBusinessError的类
    private CommonError commonError;

    //新建构造函数，方便我们使用
    //直接接收EmBusinessError的传参用于构造业务异常
    public BusinessException(CommonError commonError){
        //这里要调用super方法。因为Exception会有自身初始化的东西在里面
        super();
        this.commonError=commonError;
    }

    //接收自定义errMsg的方式构造业务异常
    public BusinessException(CommonError commonError,String errMsg){
        super();
        this.commonError=commonError;
        this.commonError.setErrorMsg(errMsg);
    }




    @Override
    public int getErrorCode() {
        return this.commonError.getErrorCode();
    }

    @Override
    public String getErrorMsg() {
        return this.commonError.getErrorMsg();
    }

    @Override
    public CommonError setErrorMsg(String Msg) {
        this.commonError.setErrorMsg(Msg);
        return this;
    }
}
