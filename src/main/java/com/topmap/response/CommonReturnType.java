package com.topmap.response;

/**
 * @Author: MaHuadong
 * @Date: 2019/4/2 14:12
 * @Version 1.0
 */
//处理接口返回数据类型,使用status和data的格式返回数据
public class CommonReturnType {

    //请求结果，success和fail
    private String status;

    //若status=success，则data内返回前端需要的json数据
    //若status=fail，则data内使用通用的错误码格式
    private Object data;

    //定义一个通用的创建方法
    public static CommonReturnType creat(Object result){
        return CommonReturnType.creat(result,"success");
    }

    //使用了函数重载的方式做了一个构造方法
    public static CommonReturnType creat(Object result,String status){
        CommonReturnType type=new CommonReturnType();
        type.setStatus(status);
        type.setData(result);
        return type;
    }


    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Object getData() {
        return data;
    }

    public void setData(Object data) {
        this.data = data;
    }
}
