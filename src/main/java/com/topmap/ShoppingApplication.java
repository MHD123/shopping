package com.topmap;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

//scanBasePackages =是为了springboot去扫描mybatis的一些配置文件，dao，service等
@SpringBootApplication(scanBasePackages = {"com.topmap"})

//MapperScan扫描dao层
@MapperScan("com.topmap.dao")
public class ShoppingApplication {

	public static void main(String[] args) {
		SpringApplication.run(ShoppingApplication.class, args);
	}

}
