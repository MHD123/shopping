package com.topmap.error;

/**
 * @Author: MaHuadong
 * @Date: 2019/4/2 14:36
 * @Version 1.0
 */
//通用错误返回方法接口
public interface CommonError {
    //定义方法
    public  int getErrorCode();

    public String getErrorMsg();

    public CommonError setErrorMsg(String Msg);
}
