package com.topmap.error;

/**
 * @Author: MaHuadong
 * @Date: 2019/4/2 14:39
 * @Version 1.0
 */
public enum EmBusinessError implements CommonError {

    //通用错误类型100001
    PARAMETER_VALIDATION_ERROR(10001,"参数不合法"),
    UNKNOW_ERROR(10002,"未知错误"),

    //20000开头为用户信息相关错误定义
    USER_NOT_EXIT(20001,"用户不存在"),

    USER_LOGIN_FAIL(20002,"用户手机号或密码不正确"),
    USER_NOT_LOGIN(20003,"用户还未登录"),
            ;


    private int errorCode;
    private String errMsg;

    

    private EmBusinessError(int errorCode, String errMsg) {
        this.errorCode=errorCode;
        this.errMsg=errMsg;
    }

    @Override
    public int getErrorCode() {
        return errorCode;
    }

    @Override
    public String getErrorMsg() {
        return errMsg;
    }

    @Override
    public CommonError setErrorMsg(String Msg) {
        this.errMsg=errMsg;
        return this;
    }
}
